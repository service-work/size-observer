/*
 * Public API Surface of size-observer
 */

export * from './lib/size-observer.module';
export * from './lib/size-observer.config';
export * from './lib/size-observer.service';
export * from './lib/size-observer';
export * from './lib/observe-size.directive';
