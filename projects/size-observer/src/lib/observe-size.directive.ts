import {
  Directive,
  AfterViewInit,
  OnDestroy,
  ElementRef,
  Output,
  EventEmitter,
  Optional,
  Inject,
} from '@angular/core';
import {
  SW_SIZE_OBSERVER_CONFIG,
  ISWSizeObserverConfig,
  defaultSizeObserverConfig,
} from './size-observer.config';
import { SizeObserverService } from './size-observer.service';
import { SizeObserver, WidthChange, HeightChange } from './size-observer';

/**
 * An Angular Directive that assigns user customizable width
 * and height classes to a DOM element based on the
 * element's display size in the browser.
 *
 * Simple usage:
 *
 * ```html
 * <div swObserveSize>
 *   <!-- div content... -->
 * </div>
 * ```
 *
 * If you wish to include `SWObserveSize` functionality in a
 * custom component, you can extend `SWObserveSize`.
 *
 * When extending `SWObserveSize`, you must call super for
 * `ngOnInit()`, `ngAfterViewInit()`, and `ngOnDestroy()`.
 */

@Directive({
  selector: '[swObserveSize]',
  exportAs: 'swObserveSize',
})
// tslint:disable-next-line: directive-class-suffix
export class SWObserveSize implements AfterViewInit, OnDestroy {
  /** The SizeObserver attached to this directive's DOM element */
  sizeObserver: SizeObserver;

  /** EventEmitter output of `WidthChange` events  */
  @Output() widthChange = new EventEmitter<WidthChange>();
  /** EventEmitter output of `HeightChange` events  */
  @Output() heightChange = new EventEmitter<HeightChange>();
  /** EventEmitter output of `WidthChange & HeightChange` events  */
  @Output() sizeChange = new EventEmitter<WidthChange & HeightChange>();

  private sizeObserverConfig: ISWSizeObserverConfig;

  constructor(
    @Optional()
    @Inject(SW_SIZE_OBSERVER_CONFIG)
    config: ISWSizeObserverConfig | null,
    public el: ElementRef<HTMLElement>,
    private sizeObserverService: SizeObserverService,
  ) {
    this.sizeObserverConfig = config || defaultSizeObserverConfig;
  }

  ngAfterViewInit() {
    this.sizeObserver = this.sizeObserverService.observe(this.el, {
      config: this.sizeObserverConfig,
      applyCSS: true,
    });

    this.sizeObserver.sizeChanges$.subscribe(size => {
      this.sizeChange.next(size);
    });

    this.sizeObserver.widthChanges$.subscribe(width => {
      this.widthChange.next(width);
    });

    this.sizeObserver.heightChanges$.subscribe(height => {
      this.heightChange.next(height);
    });
  }

  ngOnDestroy() {
    this.sizeObserver.complete();
  }
}
