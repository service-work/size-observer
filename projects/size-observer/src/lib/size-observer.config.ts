import { InjectionToken } from '@angular/core';

export const SW_SIZE_OBSERVER_CONFIG = new InjectionToken(
  'SW_SIZE_OBSERVER_CONFIG',
);

/**
 * **Important!**
 *
 * Make sure the widthBreakpoint and heightBreakpoint
 * entries are ordered from largest to smallest.
 */
export interface ISWSizeObserverConfig {
  defaultWidthClass: string;
  defaultHeightClass: string;
  widthBreakpoints: Map<number, string>;
  heightBreakpoints: Map<number, string>;
}

export const defaultSizeObserverConfig: ISWSizeObserverConfig = {
  defaultWidthClass: 'sw-container-width-xtiny',
  defaultHeightClass: 'sw-container-height-xtiny',
  widthBreakpoints: new Map([
    [1200, 'sw-container-width-xlarge'],
    [1024, 'sw-container-width-large'],
    [768, 'sw-container-width-medium'],
    [600, 'sw-container-width-small'],
    [500, 'sw-container-width-xsmall'],
    [250, 'sw-container-width-tiny'],
  ]),
  heightBreakpoints: new Map([
    [1200, 'sw-container-height-xlarge'],
    [1024, 'sw-container-height-large'],
    [768, 'sw-container-height-medium'],
    [600, 'sw-container-height-small'],
    [500, 'sw-container-height-xsmall'],
    [250, 'sw-container-height-tiny'],
  ]),
};
