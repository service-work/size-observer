import { Component } from '@angular/core';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { SWSizeObserverModule } from './size-observer.module';
import { take } from 'rxjs/operators';
import { SizeObserver } from './size-observer';
import { SizeObserverService } from './size-observer.service';

@Component({
  selector: 'test-simple-component',
  template: `
    <div style="width: 500px; height: 500px;">
      <div id="the-div" style="width: 100%; height: 100%"></div>
    </div>
  `,
})
class SimpleComponent {}

describe('SizeObserverService', () => {
  let fixture: ComponentFixture<SimpleComponent>;
  let div: HTMLDivElement;
  let service: SizeObserverService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SWSizeObserverModule],
      declarations: [SimpleComponent],
    });

    fixture = TestBed.createComponent(SimpleComponent);
    fixture.detectChanges();
    div = document.getElementById('the-div') as HTMLDivElement;
    service = TestBed.get(SizeObserverService);
  });

  it('should setup SimpleComponent', () => {
    expect(div.offsetWidth).toBe(500);
    expect(div.offsetHeight).toBe(500);
  });

  describe('observe()', () => {
    let observer: SizeObserver;

    afterEach(() => {
      observer.complete();
    });

    it('should initialize SizeObserver for "the-div"', () => {
      observer = service.observe(div);

      expect(observer.width).toBe(500);
      expect(observer.widthClass).toBe('sw-container-width-xsmall');
      expect(observer.height).toBe(500);
      expect(observer.heightClass).toBe('sw-container-height-xsmall');
    });

    it('should respect config option', () => {
      observer = service.observe(div, {
        config: {
          defaultWidthClass: 'custom-width-xtiny',
          defaultHeightClass: 'custom-height-small',
          widthBreakpoints: new Map([
            [700, 'custom-width-medium'],
            [500, 'custom-width-small'],
            [250, 'custom-width-tiny'],
          ]),
          heightBreakpoints: new Map([
            [1000, 'custom-height-large'],
            [500, 'custom-height-medium'],
          ]),
        },
      });

      expect(observer.width).toBe(500);
      expect(observer.widthClass).toBe('custom-width-small');
      expect(observer.height).toBe(500);
      expect(observer.heightClass).toBe('custom-height-medium');

      observer.complete();

      observer = service.observe(div, {
        config: {
          defaultWidthClass: 'custom-width-xtiny',
          defaultHeightClass: 'custom-height-small',
          widthBreakpoints: new Map([
            [700, 'custom-width-medium'],
            [550, 'custom-width-small'],
            [250, 'custom-width-tiny'],
          ]),
          heightBreakpoints: new Map([
            [1000, 'custom-height-large'],
            [550, 'custom-height-medium'],
          ]),
        },
      });

      expect(observer.width).toBe(500);
      expect(observer.widthClass).toBe('custom-width-tiny');
      expect(observer.height).toBe(500);
      expect(observer.heightClass).toBe('custom-height-small');
    });

    it('should respect applyCSS option', async () => {
      observer = service.observe(div, {
        applyCSS: true,
      });

      expect(div.classList).toContain(
        'sw-container-width-xsmall',
        'sw-container-height-xsmall',
      );

      let sizeChange = observer.sizeChanges$.pipe(take(1)).toPromise();

      observer.rawResizes$.next({
        contentRect: { width: 150, height: 500 },
      } as any);

      await sizeChange;

      expect(div.classList).not.toContain('sw-container-width-xsmall');

      expect(div.classList).toContain(
        'sw-container-width-xtiny',
        'sw-container-height-xsmall',
      );

      sizeChange = observer.sizeChanges$.pipe(take(1)).toPromise();

      observer.rawResizes$.next({
        contentRect: { width: 150, height: 800 },
      } as any);

      await sizeChange;

      expect(div.classList).not.toContain(
        'sw-container-width-xsmall',
        'sw-container-height-xsmall',
      );

      expect(div.classList).toContain(
        'sw-container-width-xtiny',
        'sw-container-height-medium',
      );
    });
  });
});
