import {
  Injectable,
  ElementRef,
  Inject,
  Optional,
  Renderer2,
  RendererFactory2,
} from '@angular/core';
import { SizeObserver, HeightChange, WidthChange } from './size-observer';
import {
  SW_SIZE_OBSERVER_CONFIG,
  ISWSizeObserverConfig,
  defaultSizeObserverConfig,
} from './size-observer.config';
import { tap, share } from 'rxjs/operators';

export interface ISWObserveArgs {
  config?: ISWSizeObserverConfig;
  applyCSS?: boolean;
}

@Injectable()
export class SizeObserverService {
  private config: ISWSizeObserverConfig;
  private renderer: Renderer2;

  constructor(
    @Optional()
    @Inject(SW_SIZE_OBSERVER_CONFIG)
    config: ISWSizeObserverConfig | null,
    rendererFactory: RendererFactory2,
  ) {
    this.config = config || defaultSizeObserverConfig;
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  /**
   * Accepts a DOM element and returns a SizeObserver for monitoring
   * the element's display size in the browser.
   *
   * **Options**
   * - `config` - accepts an optional `ISWSizeObserverConfig` object which
   *   will override the default config.
   * - `applyCSS` (default `false`) - when set to true, the appropriate CSS classes will
   *   automatically be applied to the DOM element based on its size.
   *   Otherwise, the DOM element will not have its CSS updated.
   *
   * @param el the element to be observed
   */
  observe(
    el: ElementRef<HTMLElement> | HTMLElement,
    options: ISWObserveArgs = {},
  ) {
    if (el instanceof ElementRef) {
      el = el.nativeElement;
    }

    const observer = new SizeObserver(el, options.config || this.config);

    if (options.applyCSS) {
      this.renderer.addClass(el, observer.widthClass);
      this.renderer.addClass(el, observer.heightClass);

      let oldSize: WidthChange & HeightChange = {
        width: observer.width,
        widthClass: observer.widthClass,
        height: observer.height,
        heightClass: observer.heightClass,
      };

      observer.sizeChanges$ = observer.sizeChanges$.pipe(
        tap(size => {
          if (size.widthClass !== oldSize.widthClass) {
            this.renderer.removeClass(el, oldSize.widthClass);
            this.renderer.addClass(el, size.widthClass);
          }

          if (size.heightClass !== oldSize.heightClass) {
            this.renderer.removeClass(el, oldSize.heightClass);
            this.renderer.addClass(el, size.heightClass);
          }

          oldSize = size;
        }),
        share(),
      );

      observer.sizeChanges$.subscribe();
    }

    return observer;
  }
}
