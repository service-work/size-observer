import { Component } from '@angular/core';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { ISWSizeObserverConfig } from './size-observer.config';
import { take } from 'rxjs/operators';
import { SizeObserver } from './size-observer';

const defaultFramingContainerConfig: ISWSizeObserverConfig = {
  defaultWidthClass: 'sw-container-width-xtiny',
  defaultHeightClass: 'sw-container-height-xtiny',
  widthBreakpoints: new Map([
    [1200, 'sw-container-width-xlarge'],
    [1024, 'sw-container-width-large'],
    [768, 'sw-container-width-medium'],
    [600, 'sw-container-width-small'],
    [500, 'sw-container-width-xsmall'],
    [250, 'sw-container-width-tiny'],
  ]),
  heightBreakpoints: new Map([
    [1200, 'sw-container-height-xlarge'],
    [1024, 'sw-container-height-large'],
    [768, 'sw-container-height-medium'],
    [600, 'sw-container-height-small'],
    [500, 'sw-container-height-xsmall'],
    [250, 'sw-container-height-tiny'],
  ]),
};

@Component({
  selector: 'test-simple-component',
  template: `
    <div style="width: 500px; height: 500px;">
      <div id="the-div" style="width: 100%; height: 100%"></div>
    </div>
  `,
})
class SimpleComponent {}

describe('SizeObserver', () => {
  let fixture: ComponentFixture<SimpleComponent>;
  let div: HTMLDivElement;
  let observer: SizeObserver;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [SimpleComponent],
    });

    fixture = TestBed.createComponent(SimpleComponent);
    fixture.detectChanges();
    div = document.getElementById('the-div') as HTMLDivElement;
    observer = new SizeObserver(div, defaultFramingContainerConfig);
  });

  it('should setup SimpleComponent', () => {
    expect(div.offsetWidth).toBe(500);
    expect(div.offsetHeight).toBe(500);
  });

  it('should calculate initial size of "the-div"', () => {
    expect(observer.width).toBe(500);
    expect(observer.widthClass).toBe('sw-container-width-xsmall');
    expect(observer.height).toBe(500);
    expect(observer.heightClass).toBe('sw-container-height-xsmall');
  });

  describe('on resize of "the-div"', () => {
    it('should monitor size', async () => {
      const prom = observer.sizeChanges$.pipe(take(1)).toPromise();

      observer.rawResizes$.next({
        contentRect: { width: 150, height: 450 },
      } as any);

      const sizeChange = await prom;

      expect(sizeChange.width).toBe(150);
      expect(sizeChange.widthClass).toBe('sw-container-width-xtiny');
      expect(sizeChange.height).toBe(450);
      expect(sizeChange.heightClass).toBe('sw-container-height-tiny');

      expect(observer.width).toBe(150);
      expect(observer.widthClass).toBe('sw-container-width-xtiny');
      expect(observer.height).toBe(450);
      expect(observer.heightClass).toBe('sw-container-height-tiny');
    });

    it('should trigger widthChange', async () => {
      const sizeChange = observer.sizeChanges$.pipe(take(1)).toPromise();
      const widthChange = observer.widthChanges$.pipe(take(1)).toPromise();

      observer.rawResizes$.next({
        contentRect: { width: 150, height: 500 },
      } as any);

      const size = await sizeChange;
      const width = await widthChange;

      expect(size.width).toBe(150);
      expect(size.widthClass).toBe('sw-container-width-xtiny');
      expect(size.height).toBe(500);
      expect(size.heightClass).toBe('sw-container-height-xsmall');

      expect(width.width).toBe(150);
      expect(width.widthClass).toBe('sw-container-width-xtiny');
    });

    it('should trigger heightChange', async () => {
      const sizeChange = observer.sizeChanges$.pipe(take(1)).toPromise();
      const heightChange = observer.heightChanges$.pipe(take(1)).toPromise();

      observer.rawResizes$.next({
        contentRect: { width: 500, height: 800 },
      } as any);

      const size = await sizeChange;
      const height = await heightChange;

      expect(size.width).toBe(500);
      expect(size.widthClass).toBe('sw-container-width-xsmall');
      expect(size.height).toBe(800);
      expect(size.heightClass).toBe('sw-container-height-medium');

      expect(height.height).toBe(800);
      expect(height.heightClass).toBe('sw-container-height-medium');
    });
  });
});
