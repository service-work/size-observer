import { Component, ViewChild } from '@angular/core';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { SWSizeObserverModule } from './size-observer.module';
import { SWObserveSize } from './observe-size.directive';

@Component({
  selector: 'test-simple-component',
  template: `
    <div style="width: 500px; height: 500px;">
      <div id="the-div" swObserveSize style="width: 100%; height: 100%"></div>
    </div>
  `,
})
class SimpleComponent {
  @ViewChild(SWObserveSize, { static: true }) directive: SWObserveSize;
}

describe('SWObserveSize', () => {
  let fixture: ComponentFixture<SimpleComponent>;
  let div: HTMLDivElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SWSizeObserverModule],
      declarations: [SimpleComponent],
    });

    fixture = TestBed.createComponent(SimpleComponent);
    fixture.detectChanges();
    div = document.getElementById('the-div') as HTMLDivElement;
  });

  it('should setup SimpleComponent', () => {
    expect(div.offsetWidth).toBe(500);
    expect(div.offsetHeight).toBe(500);
  });

  it('should apply CSS on init', async () => {
    expect(div.classList).toContain(
      'sw-container-width-xsmall',
      'sw-container-height-xsmall',
    );
  });
});
