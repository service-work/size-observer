import { NgModule } from '@angular/core';
import { SWObserveSize } from './observe-size.directive';
import { SizeObserverService } from './size-observer.service';

@NgModule({
  declarations: [SWObserveSize],
  exports: [SWObserveSize],
  providers: [SizeObserverService],
})
export class SWSizeObserverModule {}
