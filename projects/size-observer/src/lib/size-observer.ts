import { Subject, Observable } from 'rxjs';
import { map, distinctUntilChanged, tap } from 'rxjs/operators';
import { ISWSizeObserverConfig } from './size-observer.config';

export interface WidthChange {
  width: number;
  widthClass: string;
}

export interface HeightChange {
  height: number;
  heightClass: string;
}

export class SizeObserver {
  /** The px width of this element */
  width: number;
  /** The px height of this element */
  height: number;
  /** The width breakpoint class matching this element */
  widthClass: string;
  /** The height breakpoint class matching this element */
  heightClass: string;

  /** An observable stream of `WidthChange` events */
  widthChanges$: Observable<WidthChange>;
  /** An observable stream of `HeightChange` events */
  heightChanges$: Observable<HeightChange>;
  /** An observable stream of `WidthChange & HeightChange` events */
  sizeChanges$: Observable<WidthChange & HeightChange>;

  /** A stream of the raw ResizeObserver resize events for this element. */
  rawResizes$ = new Subject<ResizeObserverEntry>();

  private source$: Observable<{ width: number; height: number }>;
  private resizeObserver: ResizeObserver;

  constructor(private el: HTMLElement, private config: ISWSizeObserverConfig) {
    this.width = this.el.offsetWidth;
    this.height = this.el.offsetHeight;
    this.widthClass = this.calculateWidthCSS(this.width, this.config);
    this.heightClass = this.calculateHeightCSS(this.height, this.config);

    this.source$ = this.rawResizes$.pipe(
      map(change => ({
        width: change.contentRect.width,
        height: change.contentRect.height,
      })),
    );

    this.sizeChanges$ = this.source$.pipe(
      map(size => ({
        width: size.width,
        height: size.height,
        widthClass: this.calculateWidthCSS(size.width, this.config),
        heightClass: this.calculateHeightCSS(size.height, this.config),
      })),
      tap(size => {
        this.width = size.width;
        this.height = size.height;
        this.widthClass = size.widthClass;
        this.heightClass = size.heightClass;
      }),
    );

    this.widthChanges$ = this.sizeChanges$.pipe(
      map(size => ({ width: size.width, widthClass: size.widthClass })),
      distinctUntilChanged((x, y) => x.width === y.width),
    );

    this.heightChanges$ = this.sizeChanges$.pipe(
      map(size => ({ height: size.height, heightClass: size.heightClass })),
      distinctUntilChanged((x, y) => x.height === y.height),
    );

    this.resizeObserver = new ResizeObserver(entries =>
      this.rawResizes$.next(entries[0]),
    );

    this.resizeObserver.observe(this.el);
  }

  /**
   * You must call this method when you are done observing an
   * element's size (e.g. call this in a component's `ngOnDestroy()`
   * callback).
   *
   * Detaches the underlying `ResizeObserver` from the dom and completes
   * all associated observables.
   */
  complete() {
    this.rawResizes$.complete();
    this.resizeObserver.disconnect();
  }

  private calculateWidthCSS(width: number, config: ISWSizeObserverConfig) {
    for (const [px, klass] of config.widthBreakpoints) {
      if (width >= px) {
        return klass;
      }
    }

    return config.defaultWidthClass;
  }

  private calculateHeightCSS(height: number, config: ISWSizeObserverConfig) {
    for (const [px, klass] of config.heightBreakpoints) {
      if (height >= px) {
        return klass;
      }
    }

    return config.defaultHeightClass;
  }
}

declare class ResizeObserver {
  constructor(callback: ResizeObserverCallback);
  observe(target: Element): void;
  unobserve(target: Element): void;
  disconnect(): void;
}

type ResizeObserverCallback = (
  entries: ResizeObserverEntry[],
  observer: ResizeObserver,
) => void;

declare class ResizeObserverEntry {
  readonly target: Element;
  readonly contentRect: ContentRect;
  constructor(target: Element);
}

interface ContentRect {
  height: number;
  left: number;
  top: number;
  width: number;
}

declare const ContentRect: (target: Element) => Readonly<ContentRect>;
